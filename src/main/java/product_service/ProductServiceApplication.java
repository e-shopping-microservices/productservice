package product_service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;

import com.mongodb.reactivestreams.client.MongoClients;

import product_service.dto.ProductDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

@SpringBootApplication
public class ProductServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductServiceApplication.class, args);
	}

	
	private Sinks.Many<ProductDTO> sink;
	
	@Bean
	public Sinks.Many<ProductDTO> getSink() {
		return Sinks.many().replay().limit(2);
	}
	
	@Bean
	public Flux<ProductDTO> getSinkPub(@Autowired Sinks.Many<ProductDTO> sink) {
	    return sink.asFlux();
	}
	
	@Value("${mongo.url}")
	private String url;
	@Bean
	public ReactiveMongoTemplate  reactiveMongoTemplate() {
		return new ReactiveMongoTemplate( MongoClients.create("mongodb://localhost:27017"), "productdb" );
	}
	
}
