package product_service.controller;

import java.time.Duration;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import product_service.dto.ProductDTO;
import product_service.entity.Product;
import product_service.service.ProductService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

@RestController
@RequestMapping("/product")
public class ProductController {
	
	@Autowired
	private ProductService service;
	
	@Autowired
	private Flux<ProductDTO> pub;
	
	@GetMapping("/all")
	public Flux<ProductDTO> getAll() {
		return this.service.getAllPrd();
	}
	
	@GetMapping("/{id}")
	public Mono<ResponseEntity<Product>> get(@PathVariable("id") String id) {
		return this.service.getPrd(id)
				.map(ResponseEntity::ok) 
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@GetMapping("/price-range")
	public Flux<ProductDTO> getByPriceRange(@RequestParam("min") int min, 
			               @RequestParam("max") int max) {
		return this.service.getAllByPriceRange(min, max);
	}
	
	@PostMapping
	public Mono<ProductDTO> add(@RequestBody Mono<ProductDTO> dto) {
		return this.service.addPrd(dto);
	}

	@PutMapping("/{id}")
	public Mono<ResponseEntity<ProductDTO>> update(@PathVariable("id") String id,
			@RequestBody Mono<ProductDTO> dto) {
		return this.service.updatePrd(dto, id)
				.map(ResponseEntity::ok) 
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@DeleteMapping("/{id}")
	public Mono<Void> delete(@PathVariable("id") String id) {
		return this.service.deletePrd(id);
	}
	
	@Autowired
	private Sinks.Many<ProductDTO> sink;
	
	private void generateData() {
		Flux.range(1, 10)
		.map(i -> new ProductDTO(i+"", "Product-"+i, (new Random()).nextInt(2000)))
		.doOnNext(sink::tryEmitNext)
		.delayElements(Duration.ofSeconds(1))
		.subscribe();
	}
	
	@GetMapping(value="/stream", produces=MediaType.TEXT_EVENT_STREAM_VALUE)
	public Flux<ProductDTO> getProductStream() {
		sink.tryEmitNext(new ProductDTO("45","POOP", 4500));
		//generateData();
		return pub;
	}

}
