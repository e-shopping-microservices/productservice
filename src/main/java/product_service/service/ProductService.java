package product_service.service;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Range;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import product_service.dto.ProductDTO;
import product_service.entity.Product;
import product_service.repository.ProductRepo;
import product_service.util.ProductUtil;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepo repo;

	@Autowired
	private ReactiveMongoTemplate template;
	
	public Mono<Product> getPrd(String id) {
		/*return this.repo.findById(id)
				.map(p -> ProductUtil.toDTO(p)); */
	    
		System.out.println("I'm working fine!");
		/*return template.findById(id, Product.class)
				.map(ProductUtil::toDTO);*/
		
		Query query = new Query(); 
		query.addCriteria(new Criteria().andOperator(new Criteria("_id").is(id), new Criteria("price").gte(150)));
		query.fields().include("description");
		query.fields().exclude("_id");
		
		return template.find(query, Product.class, "product")
				.next();
		
	}
	
	public Flux<ProductDTO> getAllPrd(){  
		return this.repo.findAll()
				.map(p -> ProductUtil.toDTO(p));
	}
	
	public Flux<ProductDTO> getAllByPriceRange(int min, int max) {
		return this.repo.findByPriceBetween(min-1, max+1)
		   .map(ProductUtil::toDTO); 
	}
	
	public Mono<ProductDTO> addPrd(Mono<ProductDTO> dto){
		return dto
		.map(ProductUtil::toProduct)
		.flatMap(this.repo::insert)
		.map(ProductUtil::toDTO);
	}

	public Mono<ProductDTO> updatePrd(Mono<ProductDTO> dto, String id) {
		return this.repo.findById(id)
				.map(product -> dto
						.map(ProductUtil::toProduct)
				        .doOnNext(prd -> prd.setId(product.getId()))
				        .flatMap(prd -> this.repo.save(prd))
				        .map(ProductUtil::toDTO)
				        )
				.cast(ProductDTO.class);
	}
	
	public Mono<Void> deletePrd(String id) {
		return this.repo.deleteById(id);
	}
}
