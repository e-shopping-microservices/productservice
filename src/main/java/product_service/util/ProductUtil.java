package product_service.util;

import product_service.dto.ProductDTO;
import product_service.entity.Product;

public class ProductUtil {

	public static ProductDTO toDTO(Product product) {
		ProductDTO dto = new ProductDTO();
		dto.setDescription(product.getDescription());
		dto.setId(product.getId());
		dto.setPrice(product.getPrice());
		return dto;
	}
	
    public static Product toProduct(ProductDTO dto) {
		Product product = new Product();
		product.setDescription(dto.getDescription());
		product.setId(dto.getId());
		product.setPrice(dto.getPrice());
		return product;
	}
}
